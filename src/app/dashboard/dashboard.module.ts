import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';

// Componentes
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { ProgressComponent } from './progress/progress.component';

// Modulos
import { SharedModule } from '../shared/shared.module';

// Rutas Dashboard
import { DashboardRoutingModule } from './dashboard-routing.module';

registerLocaleData(es);

@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    ProgressComponent
  ],
  imports: [
    BrowserModule,
    DashboardRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
  ],
  exports: [
    DashboardComponent,
    HomeComponent,
    ProgressComponent
  ],
  providers: [{ provide: NZ_I18N, useValue: es_ES }]
})
export class DashboardModule { }
